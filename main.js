import FactoryDataDisplay from "./modulesJS/fetchDateOrder.js";
import * as searchComplete from "./modulesJs/searchBar.js";
import displaySinglePost from "./modulesJs/postDisplay.js";
import * as submitPosts from "./modulesJs/submitPost.js";
const contentMain = document.getElementsByClassName("columnMiddle");
const menuTagFilter = document.getElementById("tagFilter");
const radioOrderPost = document.getElementsByName("orederPost");
const searchBar = document.getElementById("searchBar");
const baseUrlJson = "http://localhost:3000/";

const ob = new FactoryDataDisplay("main");
const obOrder = new FactoryDataDisplay("order");

let currentPLace = window.location.pathname;

switch (currentPLace) {
  case "/index.html":
    
    ob.displayPosts(baseUrlJson, contentMain);
    ob.displayTagFilter(baseUrlJson, menuTagFilter, contentMain);
    radioOrderPost.forEach((item) => {
      item.addEventListener("change", function (event) {
        obOrder.displayPosts(baseUrlJson, contentMain, this.value);
      });
    });

    searchBar.addEventListener("input", searchComplete.searchValue);

    document.addEventListener("click", () => {
      searchComplete.closeAutoComplete();
    });
    break;
  case "/moduleshtml/postDisplay.html":
    const bodyContent = document.getElementById;
    displaySinglePost(baseUrlJson, contentMain);
    break;
  case "/moduleshtml/postEdit.html":
    const btnSubmit = document.getElementById("submitPost");
    btnSubmit.addEventListener('click', ()=>{
      const title = document.getElementById("txtTitle");
      const subTitle = document.getElementById("txtSubTitle");
      const body = document.getElementById("txtBody");

      let validation = submitPosts.validateFields(title,subTitle,body);

      submitPosts.createNewPost(validation,title,subTitle,body);

      if(validation){
        window.location.replace("../index.html");
      }

    });
    break;

  default:
    alert("PAGE NOT FOUND");
    break;
}
