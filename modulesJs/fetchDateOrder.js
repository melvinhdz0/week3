class DataDisplay{
  displayPosts(baseURL, parentDiv) {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      `${baseURL}posts?_sort=createDate&_order=desc&_limit=3`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        let postContentFeature = document.createElement("div");
        postContentFeature.setAttribute("class", "postContentFeature");
        postContentFeature.innerHTML = "<label class = 'featureSeparator'>Most Recent</label>";
        parentDiv[0].appendChild(postContentFeature);
        for (let i = 0; i < 3; i++) {
          let postContent = document.createElement("div");
          postContent.setAttribute("class", "postContent");
          postContent.innerHTML = `<h2 class = "titlePost" ><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">${data[i].title}</a></h2>
        <h3 class = "subTitlePost">${data[i].subTitle}</h3>
        <p class = "postBody">${data[i].body}</p>
        <div class = "postReaction"><i class="far fa-heart"><input class="value" type="hidden" value=${data[i].id}>${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
          postContentFeature.appendChild(postContent);
        }
      })
      .catch((error) => console.log("error", error));

    fetch(
      `${baseURL}posts?_sort=createDate&_order=desc&_start=3&_limit=12`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        let postContentNoFeature = document.createElement("div");
        postContentNoFeature.setAttribute("class", "postContentFeature");
        postContentNoFeature.innerHTML = "<label class = 'featureSeparator'>Old</label>";
        parentDiv[0].appendChild(postContentNoFeature);
        for (let i = 0; i < data.length; i++) {
          let postContent = document.createElement("div");
          postContent.setAttribute("class", "postContent");
          postContent.innerHTML = `<h2 class = "titlePost" ><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">>${data[i].title}</a></</h2>
        <h3 class = "subTitlePost">${data[i].subTitle}</h3>
        <p class = "postBody">${data[i].body}</p>
        <div class = "postReaction"><i class="far fa-heart"><input class="value" type="hidden" value=${data[i].id}> ${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
          postContentNoFeature.appendChild(postContent);
        }
      })
      .catch((error) => console.log("error", error));
  }

  displayTagFilter(baseURL, parentDiv, parentDivBody) {
    let tagsSelected = [];
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(`${baseURL}tags`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        data.forEach((dataMap) => {
          let divTagMenu = document.createElement("div");
          divTagMenu.setAttribute("class", "divTagMenuItem");
          parentDiv.appendChild(divTagMenu);
          let inputCheckBox = document.createElement("input");
          inputCheckBox.setAttribute("type", "checkbox");
          inputCheckBox.setAttribute("value", dataMap.id);
          divTagMenu.appendChild(inputCheckBox);
          let labelTag = document.createElement("label");
          labelTag.innerHTML = dataMap.name;
          divTagMenu.appendChild(labelTag);

          inputCheckBox.addEventListener("change", function () {
            const radiobtn1 = document.getElementById("radioOrder1");
            const radiobtn2 = document.getElementById("radioOrder2");
            console.log(radiobtn1, radiobtn2);
            const requestOptions = {
              method: "GET",
              redirect: "follow",
            };

            if (this.checked) {
              tagsSelected.push(this.value);

              radiobtn1.disabled = true;
              radiobtn2.disabled = true;
              //uppdate by tags

              for (
                let x = parentDivBody[0].childNodes.length - 1; x >= 0; x--) {
                parentDivBody[0].removeChild(parentDivBody[0].childNodes[x]);
              }

              let urlSearchQuery = "";
              tagsSelected.forEach((tag) => {
                urlSearchQuery += `(?=.*${tag})`;
              });
              fetch(
                `${baseURL}posts?tags_like=(${urlSearchQuery})`,
                requestOptions
              )
                .then((response) => response.json())
                .then((data) => {
                  for (let i = 0; i < data.length; i++) {
                    let postContent = document.createElement("div");
                    postContent.setAttribute("class", "postContent");
                    postContent.innerHTML = `<h2 class = "titlePost"><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">>${data[i].title}</a></</h2>
                    <h3 class = "subTitlePost">${data[i].subTitle}</h3>
                    <p class = "postBody">${data[i].body}</p>
                    <div class = "postReaction"><i class="far fa-heart"> ${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
                    parentDivBody[0].appendChild(postContent);
                  }
                })
                .catch((error) => console.log("error", error));
            } else {
              let index = tagsSelected.indexOf(this.value);
              tagsSelected.splice(index, 1);

              for (
                let x = parentDivBody[0].childNodes.length - 1; x >= 0; x--) {
                parentDivBody[0].removeChild(parentDivBody[0].childNodes[x]);
              }

              if (tagsSelected.length == 0) {
                radiobtn1.disabled = false;
                radiobtn2.disabled = false;
                fetch(
                  `${baseURL}posts?_sort=createDate&_order=desc&_limit=3`,
                  requestOptions
                )
                  .then((response) => response.json())
                  .then((data) => {
                    let postContentFeature = document.createElement("div");
                    postContentFeature.setAttribute(
                      "class",
                      "postContentFeature"
                    );
                    postContentFeature.innerHTML = "<label class = 'featureSeparator'>Most Recent</label>";
                    parentDivBody[0].appendChild(postContentFeature);
                    for (let i = 0; i < 3; i++) {
                      let postContent = document.createElement("div");
                      postContent.setAttribute("class", "postContent");
                      postContent.innerHTML = `<h2 class = "titlePost"><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">>${data[i].title}</a></</h2>
                    <h3 class = "subTitlePost">${data[i].subTitle}</h3>
                    <p class = "postBody">${data[i].body}</p>
                    <div class = "postReaction"><i class="far fa-heart"> ${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
                      postContentFeature.appendChild(postContent);
                    }
                  })
                  .catch((error) => console.log("error", error));

                fetch(
                  `${baseURL}posts?_sort=createDate&_order=desc&_start=3&_limit=12`,
                  requestOptions
                )
                  .then((response) => response.json())
                  .then((data) => {
                    let postContentNoFeature = document.createElement("div");
                    postContentNoFeature.setAttribute(
                      "class",
                      "postContentFeature"
                    );
                    postContentNoFeature.innerHTML = "<label class = 'featureSeparator'>Old</label>";
                    parentDivBody[0].appendChild(postContentNoFeature);
                    for (let i = 0; i < data.length; i++) {
                      let postContent = document.createElement("div");
                      postContent.setAttribute("class", "postContent");
                      postContent.innerHTML = `<h2 class = "titlePost"><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">>${data[i].title}</a></h2>
                    <h3 class = "subTitlePost">${data[i].subTitle}</h3>
                    <p class = "postBody">${data[i].body}</p>
                    <div class = "postReaction"><i class="far fa-heart"> ${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
                      postContentNoFeature.appendChild(postContent);
                    }
                  })
                  .catch((error) => console.log("error", error));
              } else {
                let urlSearchQuery = "";
                tagsSelected.forEach((tag) => {
                  urlSearchQuery += `(?=.*${tag})`;
                });
                fetch(
                  `${baseURL}posts?tags_like=(${urlSearchQuery})`,
                  requestOptions
                )
                  .then((response) => response.json())
                  .then((data) => {
                    for (let i = 0; i < data.length; i++) {
                      let postContent = document.createElement("div");
                      postContent.setAttribute("class", "postContent");
                      postContent.innerHTML = `<h2 class = "titlePost"><a href = "./moduleshtml/postDisplay.html?id=${data[i].id}">>${data[i].title}</a></</h2>
                    <h3 class = "subTitlePost">${data[i].subTitle}</h3>
                    <p class = "postBody">${data[i].body}</p>
                    <div class = "postReaction"><i class="far fa-heart"> ${data[i].likes}</i><i class="far fa-calendar-alt"> ${data[i].createDate}</i></div>`;
                      parentDivBody[0].appendChild(postContent);
                    }
                  })
                  .catch((error) => console.log("error", error));
              }
            }
          });
          //end update

        });
      })
      .catch((error) => console.log("error", error));
  }

}

class DataDisplayOrder{
  displayPosts(baseURL, parentDiv, direction) {
    for (let x = parentDiv[0].childNodes.length - 1; x >= 0; x--) {
      parentDiv[0].removeChild(parentDiv[0].childNodes[x]);
    }

    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      `${baseURL}posts?_sort=createDate&_order=${direction}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        data.forEach((element) => {
          let postContent = document.createElement("div");
          postContent.setAttribute("class", "postContent");
          postContent.innerHTML = `<h2 class = "titlePost"><a href = "./moduleshtml/postDisplay.html?id=${element.id}">>${element.title}</a></</h2>
          <h3 class = "subTitlePost">${element.subTitle}</h3>
          <p class = "postBody">${element.body}</p>
          <div class = "postReaction"><i class="far fa-heart"> ${element.likes}</i><i class="far fa-calendar-alt"> ${element.createDate}</i></div>`;
          parentDiv[0].appendChild(postContent);
        });
      })
      .catch((error) => console.log("error", error));
  }

}

export default class FactoryDataDisplay {
  constructor(tipo) {
    switch (tipo) {
      case "main":
        return new DataDisplay();

      case "order":
        return new DataDisplayOrder();

      default:
        return null;
    }
  }
}