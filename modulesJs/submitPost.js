export function validateFields(title, subTitle, body) {
  let passValidation = true;

  if (title.value == null || title.value.trim() == "") {
    title.style.border = "2px solid #ff4d4d";
    title.setAttribute("placeholder", "Requiered Field");
    passValidation = false;
  }

  if (subTitle.value == null || subTitle.value.trim() == "") {
    subTitle.style.border = "2px solid #ff4d4d";
    subTitle.setAttribute("placeholder", "Requiered Field");
    passValidation = false;
  }

  if (body.value == null || body.value.trim() == "") {
    body.style.border = "2px solid #ff4d4d";
    body.setAttribute("placeholder", "Requiered Field");
    passValidation = false;
  }

  return passValidation;
}

export function createNewPost(validation, title, subtitle, body) {
  const d = new Date();

  if (validation) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const post = JSON.stringify({
      title: title.value,
      subTitle: subtitle.value,
      image: "",
      body: body.value,
      createDate: `${d.getFullYear()}/${d.getMonth()}/${d.getDate()}`,
      likes: 2,
      author: 1,
      tags: [1, 5],
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: post,
      redirect: "follow",
    };

    fetch("http://localhost:3000/posts", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        if(validation){
            window.location.replace("../index.html");
          }    
      })
      .catch((error) => console.log("error", error));
  }
}
