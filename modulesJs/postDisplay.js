function getValueUrl(){
    const urlString = window.location.search;
    const idPost = new URLSearchParams(urlString);
    const id = idPost.get('id');
    return id;
}

export default function displaySinglePost(baseURL,parentDivBody) {
    
    const idPost = getValueUrl();
    
    const requestOptions = {
        method: "GET",
        redirect: "follow",
      };
  
      fetch(
        `${baseURL}posts?id=${idPost}`,
        requestOptions
      )
      .then((response) => response.json())
      .then((data) => {
          console.log(data);
          let postContent = document.createElement("div");
          postContent.setAttribute("class", "postContent");
          postContent.innerHTML = `<h2 class = "titlePost">${data[0].title}</h2>
          <h3 class = "subTitlePost">${data[0].subTitle}</h3>
          <p class = "postBody">${data[0].body}</p>
          <img class="imgenPost" src="${data[0].image}" />
          <div class = "postReaction"><i class="far fa-heart"> ${data[0].likes}</i><i class="far fa-calendar-alt"> ${data[0].createDate}</i></div>`;
          parentDivBody[0].appendChild(postContent);
      })
      .catch((error) => console.log("error", error));
} 


