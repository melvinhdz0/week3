export function closeAutoComplete(elementList) {
  try{
    let list = document.getElementsByClassName("autocomplete-items");

  for (i = 0; i < list.length; i++) {
    if (elementList != list[i] && elementList != searchBar) {
      list[i].parentNode.removeChild(list[i]);
    }
  }
  }catch(e){}
}

const debounce = (fn, delay) => {
  let timer;
  return function () {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn();
    }, delay);
  };
};

function generateAutoComplete() {
  closeAutoComplete();

  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  
  fetch(`${baseURL}posts?_sort=createDate&_order=desc&_limit=3`, requestOptions)
    .then((response) => response.json())
    .then((data) => {
      const searchBar = document.getElementById("searchBar");
      let divItems,
        completeBar = document.createElement("div");
      completeBar.setAttribute("class", "autocomplete-items");
      searchBar.parentNode.appendChild(completeBar);

      data.array.forEach((element) => {
        if (
          element.title.substring(0, searchBar.value.length).toUpperCase() ==
          searchBar.value.toUpperCase()
        ) {
          divItems = document.createElement("div");
          divItems.innerHTML =
            "<strong>" +
            element.title.substring(0, searchBar.value.length) +
            "</strong>";
            divItems.innerHTML += element.title.substring(searchBar.value.length);
            divItems.innerHTML +=
            "<input type = 'hidden' value = '" + cities[i].woeidFetch + "'/>";
          //event listener
          //
          completeBar.appendChild(divItems);
        }
      });
    })
    .catch((error) => console.log("error", error));
}

const searchValue = debounce(generateAutoComplete,500);

export default searchValue;